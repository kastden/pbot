#!/usr/bin/env python3

import sys
import signal
import threading
import time

from pbot import config
from pbot import log
from pbot.bot import Bot


def quit(signum, frame):
    for bot in bots:
        bot.disconnect()
    log.close()
    sys.exit()

for s in [signal.SIGTERM, signal.SIGINT]:
    signal.signal(s, quit)
    signal.siginterrupt(s, False)

bots = []
for c in config.bots:
    if not c.autoconnect:
        continue
    bot = Bot(c)
    bots.append(bot)
    threading.Thread(target=bot.connect, daemon=True).start()

while True:
    time.sleep(60)
    log.flush()
