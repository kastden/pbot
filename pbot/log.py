#!/usr/bin/env python3

import os
import os.path
import sys
from datetime import datetime

config_directory = os.path.abspath(
    os.path.join(os.path.dirname(os.path.realpath(__file__)),
                 '../'))
log_path = os.path.join(config_directory, 'pbot.log')

logfile = open(log_path, 'a')
stdout = os.isatty(sys.stdout.fileno())


def write(text):
    line = '%s %s' % (datetime.now(), text)
    if 0 <= line.rfind('\n') < len(line) - 1:
        line += '\n\n'
    else:
        line += '\n'

    if stdout:
        print(line, end='')
    logfile.write(line)


def flush():
    logfile.flush()


def close():
    logfile.close()
