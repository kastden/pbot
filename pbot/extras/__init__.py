#!/usr/bin/env python3

import requests
s = requests.Session()

from pbot.extras.royal import burger_of_the_month
from pbot.extras import colors


def liar(bot, target, nick, command, text):
    bot.say(target, "Serial over-promiser Peter Molyneux promises "
        "to stop over-promising")


def royal(bot, target, nick, command, text):
    burger = burger_of_the_month()
    bot.say(target, burger)

if __name__ == "__main__":
    pass
