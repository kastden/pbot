#!/usr/bin/env python3

from bs4 import BeautifulSoup as bs4

from pbot.extras import s


def burger_of_the_month():
    r = s.get("http://royalburger.no/")
    try:
        r.raise_for_status()
    except:
        return "Failure. Is http://royalburger.no/ up?"

    soup = bs4(r.text, "html.parser")
    special_burgers = soup.findAll('div',
                                   {'class': 'highlight-burger'})

    special_burger = None
    for burger in special_burgers:
        if burger.text == "Månedens burger":
            special_burger = burger.parent

    if not special_burger:
        return "Fant ikke månedens burger. :("

    name = special_burger.find('h3',
                               {'class': 'highlight'}).string.strip()
    p = special_burger.findAll('p')
    allergens = p[0].text.strip()
    toppings = p[1].text.strip()

    m = ("Månedens burger på Royal: {}. "
        "{}. Allergener: {}").format(name, toppings, allergens)
    return m


if __name__ == "__main__":
    burger_of_the_month()
