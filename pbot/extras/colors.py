#!/usr/bin/env python3

colors = [('00', 'white'),
          ('01', 'black'),
          ('02', 'blue'),
          ('03', 'green'),
          ('04', 'red'),
          ('05', 'brown'),
          ('06', 'purple'),
          ('07', 'orange'),
          ('08', 'yellow'),
          ('09', 'lime'),
          ('10', 'teal'),
          ('11', 'cyan'),
          ('12', 'royal'),
          ('13', 'pink'),
          ('14', 'grey'),
          ('15', 'silver')
          ]

background = {}
foreground = {}

for c in colors:
    foreground[c[1]] = '\x03{}'.format(c[0])

print(foreground)

reset = "\x0F"
bold = "\x02"
italic = "\x1D"
underlined = "\x1F"