#!/usr/bin/env python3

import sys
import importlib

import pbot.extras
from pbot import config


def reload(bot, target, nick, command, text):
    if config.settings['owner'] == nick:
        for i in range(0, 2):
            importlib.reload(pbot.extras)
            importlib.reload(sys.modules[__name__])
        bot.say(target, 'Bot reloaded.')


def die(bot, target, nick, command, text):
    bot.disconnect()
    sys.exit()


handlers = {
    'reload': reload,
    'die': die,
    'liar': pbot.extras.liar,
    'royal': pbot.extras.royal,
}
